# M08 LoopBack Example

A simple guest book using Node, Express, BootStrap, EJS

## How to use

Open a command window in your c:\44563\m08\app_folder1 folder.

Run npm install to install all the dependencies in the package.json file.

Run node . to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> node .

```

Point your browser to `http://localhost:3000/explorer`
